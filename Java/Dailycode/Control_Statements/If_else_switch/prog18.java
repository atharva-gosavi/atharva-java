class Prog18{
        public static void main(String[] args){
                String friends = "Kanha";
                System.out.println("Before Switch");
                switch(friends){
                        case "Anish":
                                System.out.println("Barclays");
                                break;
                        case "Kanha":
                                System.out.println("BMC software");
                                break;
                        case "Rahul":
                                System.out.println("Infosys");
                                break;
                        default:
                                System.out.println("In the deault state");
                }
                System.out.println("After Switch");
        }
}
