class Prog17{
        public static void main(String[] args){
                char num = 'B';
                System.out.println("Before Switch");
                switch(num){
                        case 'A':
                                System.out.println("A");
                                break;
                        case 65:
                                System.out.println("65");
                                break;
			case 'B':
                                System.out.println("B");
                                break;
                        case 66:
                                System.out.println("66");
                                break;
                        default:
                                System.out.println("In the deault state");
                }
                System.out.println("After Switch");
        }
}
