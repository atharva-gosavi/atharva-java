class Prog16{
        public static void main(String[] args){
                char num = 'B';
                System.out.println("Before Switch");
                switch(num){
                        case 'A':
                                System.out.println("A");
                                break;
                        case 'B':
                                System.out.println("B");
                                break;
                        case 'C':
                                System.out.println("C");
                                break;
                        default:
                                System.out.println("In the deault state");
                }
                System.out.println("After Switch");
        }
}
