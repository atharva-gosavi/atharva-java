class Prog15{
	public static void main (String[] args){
		int marks = 95;
		float percent = 95.00f;
		char grade = 'A';
		boolean qualified = true;
		double income = 950000;
		System.out.println("Marks obtained "+marks);
		System.out.println("Percentage = "+percent);
		System.out.println("Grade obtained = "+grade);
		System.out.println("Promoted to next class = "+qualified);
		System.out.println("Annual Income = "+income);

	}
}
