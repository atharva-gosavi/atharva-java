import java.io.*;
class ArrayBasic{
	public static void main(String[] args)throws IOException{
		int arr[] = new int[10];
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		for(int i = 0; i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		for(int i = 0; i<arr.length ;i++){
			System.out.print(arr[i]+" ");
		}
	}
}
