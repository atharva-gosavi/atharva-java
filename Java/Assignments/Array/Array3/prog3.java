import java.util.*;
class Array3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size:");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements in array");
		for(int i =0; i<size ; i++){
			arr[i]= sc.nextInt();
		}System.out.println("Enter the Specific No. to find: ");
		int num = sc.nextInt();
		int cnt = 0;
		for(int i =0; i<size ; i++){
			if(arr[i]==num){
				cnt++;
			}
		}
		System.out.println("Number "+num+" occured "+cnt+" times in array");
	}
}
