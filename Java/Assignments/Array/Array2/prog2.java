import java.util.*;
class Array2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size: ");
		int size = sc.nextInt();
		int arr[]= new int[size];
		System.out.println("Enter the array elements:");
		for(int i = 0; i<size; i++){
			arr[i]= sc.nextInt();
		}
		int sum = 0;
		System.out.print("Elements divisible by 3: ");
		for(int i = 0; i<size; i++){
			if(arr[i]%3==0){
				System.out.print(arr[i]+" ");
				sum = sum + arr[i];
			}
		}
		System.out.println("sum of elemnts divisible by 3 is : "+sum);

	}
}
