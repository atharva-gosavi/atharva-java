import java.util.*;
class Array2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size: ");
		int size = sc.nextInt();
		int arr[]= new int[size];
		System.out.println("Enter the array elements:");
		for(int i = 0; i<size; i++){
			arr[i]= sc.nextInt();
		}
		int cnt = 0;
		System.out.print("Even numbers ");
		for(int i = 0; i<size; i++){
			if(arr[i]%2==0){
				System.out.print(arr[i]+" ");
				cnt++;
			}
		}
		System.out.println("Count of even elements is : "+cnt);

	}
}
