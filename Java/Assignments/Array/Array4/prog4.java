import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size =  sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Array Elements: ");
		for(int i = 0; i<size ; i++){
			arr[i]= sc.nextInt();
		}
		System.out.println("Enter the no. to check occurence:");
		int num = sc.nextInt();
		int cnt = 0;
		for(int i = 0; i<size ; i++){
			if(arr[i]==num){
				cnt++;
			}
		}
		if(cnt>2){
			System.out.println(num + " occurs more than 2 times");
		}
		else if(cnt==2){
			System.out.println(num + " occurs 2 times");
		}
		else{
			System.out.println(num + " occurs less than 2 times");
		}
	}
}
