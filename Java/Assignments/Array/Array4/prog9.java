import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size =  sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Array Elements: ");
		for(int i = 0; i<size ; i++){
			arr[i]= sc.next().charAt(0);
		}
		for(int i = 0; i<size ; i++){
			if(arr[i]>='A' && arr[i]<='Z'){
				System.out.println("# ");
			}
			else{
				System.out.println((char)arr[i]+" ");
			}
		}
		
	}
}
