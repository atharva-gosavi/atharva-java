import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size =  sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Array Elements: ");
		for(int i = 0; i<size ; i++){
			arr[i]= sc.next().charAt(0);
		}
		int cnt1=0;
		int cnt2=0;
		for(int i = 0; i<size ; i++){
			if(arr[i] == 'a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'||arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
				cnt1++;
			}
			else{
				cnt2++;
			}
		}
		System.out.println("Count of vowel is "+cnt1);
		System.out.println("Count of consonants is "+cnt2);
	}
}
