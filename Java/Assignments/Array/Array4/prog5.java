import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size =  sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Array Elements: ");
		for(int i = 0; i<size ; i++){
			arr[i]= sc.nextInt();
		}
		int temp=0;
		for(int i = 0; i<size/2 ; i++){
			temp = arr[i];
			arr[i]= arr[size-1-i];
			arr[size-1-i]= temp;
		}
		System.out.println("Reversed array");
		for(int i = 0; i<size ; i++){
			System.out.println(arr[i]);
		}
	}
}
