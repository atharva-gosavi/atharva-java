import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size =  sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Array Elements: ");
		for(int i = 0; i<size ; i++){
			arr[i]= sc.next().charAt(0);
		}
		System.out.println("Enter the character to check occurence:");
		char num = sc.next().charAt(0);
		int cnt = 0;
		for(int i = 0; i<size ; i++){
			if(arr[i]==num){
				cnt++;
			}
		}
		System.out.println(num + " occurs "+ cnt);
	}
}
