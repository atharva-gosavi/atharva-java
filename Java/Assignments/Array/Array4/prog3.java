import java.io.*;
class Array4{
	public static void main(String[] args)throws IOException{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Enter the size of the array:");
			int size = Integer.parseInt(br.readLine());
			int arr[] = new int[size];
			System.out.println("Enter the element of the array:");
			for(int i = 0; i<size;i++){
				arr[i]= Integer.parseInt(br.readLine());
			}
			int max1=0;
			int max2 = 0;
			for(int i = 0; i<size ; i++){
				if(arr[i]>max1){
					max2=max1;
					max1 = arr[i];
				}
				else if(arr[i]>max2 && arr[i]<max1){
					max2 = arr[i];
				}	
			}
			System.out.println(max2);
	}
}
