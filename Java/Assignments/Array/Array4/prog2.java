import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size =  sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Array Elements: ");
		for(int i = 0; i<size ; i++){
			arr[i]= sc.nextInt();
		}
		int min = arr[0];
		int max = arr[0];
		for(int i = 0; i<size ; i++){
			if(arr[i]<min){
				min = arr[i];
			}
			if(arr[i]>max){
				max = arr[i];
			}
		}
		int diff = max - min;
		System.out.println("The difference between maximum and minimum is: "+diff);
	}
}
