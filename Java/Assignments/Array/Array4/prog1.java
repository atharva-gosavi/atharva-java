import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size =  sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Array Elements: ");
		for(int i = 0; i<size ; i++){
			arr[i]= sc.nextInt();
		}
		int sum = 0;
		for(int i = 0; i<size ; i++){
			sum = sum+ arr[i];
		}
		int avg = sum/size;
		System.out.println("Array Elements Average is: "+avg);

	}
}
