import java.util.*;
class Array5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter the elements in the array");
		for(int i = 0; i<size; i++){
			arr[i]= sc.nextInt();
		}
		for(int i = 0;i<size;i++){
			int cnt=0;
			while(arr[i]>0){
				arr[i]=arr[i]/10;
				cnt++;
			}
			System.out.print(cnt+" ,");
		}
	}
}
