import java.util.*;
class MixedPattern{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of rows : ");
		int row = sc.nextInt();
		for(int i = 1; i<=row; i++){
			for(int sp = 1 ; sp>i; sp++){
				System.out.print("\t");
			}
			int num = i;
			if(row%2==1){
				for(int j = row; j>=i; j--){
					System.out.print((char)(num+64)+"\t");
					num++;
				}
			}
			else{
				for(int j = row; j>=i; j--){
					System.out.print((char)(num+96)+"\t");
					num++;
				}
			}
			System.out.println();
		}
	}
}
