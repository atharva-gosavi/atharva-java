import java.util.*;
class Prog10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no. of rows");
		int row = sc.nextInt();
		for(int i = 1; i<=row; i++){
			int num = row-i+1;
			int cap = 64+num;
			int small = 96+num;
			for (int j=1; j<=row-i+1;j++){
				if(i%2==1){
					System.out.print((char)(small)+ " ");
					small--;
				}else{	
					System.out.print((char)(cap) + " ");
					cap--;
				}
			}
			System.out.println();
		}
	}
}
