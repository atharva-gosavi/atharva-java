import java.util.*;
class Prog5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no. of rows");
		int row = sc.nextInt();
		for(int i = 1; i<=row; i++){
			char cap = 'A';
			char small = 'a';
			for (int j=1; j<=row-i+1;j++){
				if(i%2==1){
					System.out.print(cap + " ");
					cap++;
				}else{	
					System.out.print(small + " ");
					small++;
				}
			}
			System.out.println();
		}
	}
}
