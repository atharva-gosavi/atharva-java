import java.util.*;
class Prog8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no. of rows");
		int row = sc.nextInt();
		for(int i = 1; i<=row; i++){
			int num = row-i+1;
			int ch = 64+num;
			for (int j=1; j<=row-i+1;j++){
				if(i%2==1){
					System.out.print(num + " ");
					num--;
				}else{	
					System.out.print((char)(ch) + " ");
					ch--;
				}
			}
			System.out.println();
		}
	}
}
