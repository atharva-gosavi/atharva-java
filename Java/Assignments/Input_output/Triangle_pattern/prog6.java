import java.util.*;
class Prog6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no. of rows");
		int row = sc.nextInt();
		for(int i = 1; i<=row; i++){
			int num = 1;
			char ch = 'a';
			for (int j=1; j<=row-i+1;j++){
				if(j%2==1){
					System.out.print(num + " ");
					num++;
				}else{	
					System.out.print(ch + " ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}
