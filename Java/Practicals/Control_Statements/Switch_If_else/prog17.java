class Prog17{
	public static void main(String[] args){
		int num = 10000;
		switch(num){
			case 15000:
				System.out.println("Your destination is Jammu and Kashmir");
				break;
			case 10000:
				System.out.println("Your destination is Manali");
				break;
			case 6000:
				System.out.println("Your destination is Amritsar");
				break;
			case 2000:
				System.out.println("Your destination is Mahabaleshwar");
				break;
			default:
				System.out.println("Other budgets try next time");
		}
	}
}
