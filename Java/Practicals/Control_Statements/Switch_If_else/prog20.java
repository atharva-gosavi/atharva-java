class Prog20{
	public static void main(String[] args){
		int num = 50000;
		switch(num){
			case 1000:
				System.out.println("Pay by cash");
				break;
			case 5000:
				System.out.println("Pay via Online banking");
				break;
			case 10000:
				System.out.println("Pay by cheque");
				break;
			case 50000:
				System.out.println("Pay in Installments");
				break;
			default:
				System.out.println("Not affordable");
		}
	}
}
