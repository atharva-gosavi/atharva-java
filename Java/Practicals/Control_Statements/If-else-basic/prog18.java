class Prog18{
	public static void main(String[] args){
	 	float p= 80.00f;
		if(p>=75 && p<=100){
			System.out.println("First class with distinction");
		}else if(p<=65 && p<75){
			System.out.println("First class");
		}else if(p>=55 && p<65){
			System.out.println("Second class");
		}else if(p>=40 && p<55){
			System.out.println("Pass");
		}else{
			System.out.println("Fail");
		}
	}
}
