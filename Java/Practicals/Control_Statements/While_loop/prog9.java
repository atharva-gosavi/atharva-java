class Prog9{
	public static void main(String[] args){
		int count1=0;
		int count2=0;
		long num = 214367689l;
		while(num>0){
			long rem = num%10;
			num = num/10;
			if(rem%2==0){
				count1++;
			}else{
				count2++;
			}
		}
		System.out.println("Even count = "+count1);
		System.out.println("Odd count = "+count2);
	}
}
