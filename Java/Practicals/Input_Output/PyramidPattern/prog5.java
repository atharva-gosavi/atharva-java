import java.io.*;
class PyramidPattern{
	public static void main(String[] args)throws IOException{
		System.out.println("Enter number of rows: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int row = Integer.parseInt(br.readLine());
		for(int i = 1; i<=row;i++){
			for(int sp= row; sp>i; sp--){
				System.out.print("\t");
			}
			int num = 1;
			for(int j = 1; j<=i*2-1; j++){
				if(j<i){
					System.out.print(num +"\t");	
					num++;
				}
				else{
					System.out.print(num-- +"\t");	
				}
			}
			System.out.println();
		}
	}
}
