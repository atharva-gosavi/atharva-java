import java.io.*;
class PyramidPattern{
	public static void main(String[] args)throws IOException{
		System.out.println("Enter number of rows: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int row = Integer.parseInt(br.readLine());
		int num = 1;
		for(int i = 1; i<=row;i++){
			for(int sp= row; sp>i; sp--){
				System.out.print("\t");
			}
			for(int j = 1; j<=i*2-1; j++){
				System.out.print(num + "\t");
				num++;
			}
			System.out.println();
		}
	}
}
