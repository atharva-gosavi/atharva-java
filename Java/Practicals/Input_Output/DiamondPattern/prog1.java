import java.io.*;
class DiamondPattern{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the no. of rows:");
                int row = Integer.parseInt(br.readLine());
		br.close();
                int col =0;
                int s =0;
                int num =1;
                for(int i =1; i<row*2;i++){
                        if(i<=row){
                                s=row-i;
                                col= i*2-1;
                        }else{
                                s= i-row;
                                col-=2;
                        }
                        for(int sp=1;sp<=s;s++){
                                System.out.print("\t");
                        }
                        for(int j=1;j<=col;j++){
                                if(j<col+1/2){
                                        System.out.print(num+"\t");
                                        num++;
                                }
                                else{
                                        System.out.print(num+"\t");
                                        num--;
                                }
                        }
                        System.out.println();
                }
        }
}
