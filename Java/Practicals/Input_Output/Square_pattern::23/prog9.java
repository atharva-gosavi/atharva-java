import java.io.*;
class SquarePat{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		int num1 = row*row;
		int num2 =(row-2+row)*row;
		for(int i =1; i<=row; i++){
			int min = i*2;
			for(int j = 1; j<=row; j++){
			if(i==1 || i==row){
				if(j%2==1){
				System.out.print(num1+"\t");
				num1-=min;
				}
				else{
					System.out.print("@\t");
				}
			}
			else{
				if(j%2==1){
					System.out.print(num2+"\t");
					num2-=min;
				}
				else{
					System.out.print("@\t");
				}
			}
			}
			System.out.println();
		}
	}
}
