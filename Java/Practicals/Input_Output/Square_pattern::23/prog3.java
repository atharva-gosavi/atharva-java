import java.io.*;
class SquarePat{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		int num = row;
		for(int i =1; i<=row; i++){
			for(int j = 1; j<=row; j++){
			if(i%2==1 && j%2==1){
				System.out.print((char)(num+96)+"\t");
			}
			else if(i%2==0 &&j%2==0){
				System.out.print(num + "\t");
			}
			else if(i%2==0 &&j%2==1){
				System.out.print(num + "\t");
			}
			else{
				System.out.print((char)(num+96)+"\t");
			}
			num++;
			}
			System.out.println();
		}
	}
}
