import java.io.*;
class SquarePat{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		int num = row;
		for(int i =1; i<=row; i++){
			for(int s = 1; s<=row-i;s++){
				System.out.print((char)(num+96)+"\t");
				num++;
			}
			for(int j = 1; j<=i; j++){
				System.out.print((char)(num+64)+"\t");			
				num++;
			}
			System.out.println();
		}
	}
}
