import java.io.*;
class SquarePat{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i =1; i<=row; i++){
			int num = row*row;
			for(int j = 1; j<=row; j++){
			if(i%2==0 &&j%2==0){
				num-=5;
				System.out.print(num + "\t");
			}
			else if(i%2==0 &&j%2==1){
				System.out.print(num + "\t");
			}
			else{
				System.out.print(num+"\t");
				num--;
			}
			}
			System.out.println();
		}
	}
}
