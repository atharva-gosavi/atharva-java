import java.util.*;
class SquarePat{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no. of rows:");
		int row = sc.nextInt();
		for(int i = 1; i<=row;i++){
			int num = row;
			for(int j = 1; j<=row;j++){
				if(i%2==1&&j%2==0){
				System.out.print((char)(64+num)+" ");
				num--;
				}
				else if(i%2==1&&j%2==1){
				System.out.print("# ");
				}
				else if(i%2==0&&j%2==0){
				System.out.print("# ");
				}
				else{
					System.out.print((char)(num+64)+" ");
					num--;
				}
			}System.out.println();
 		}
	}
}
