import java.io.*;
class SpacePattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows:");
		int row = Integer.parseInt(br.readLine());
		int num = row;
		for(int i =1; i<=row; i++){
			for(int space= 1 ; space <i; space++){
				System.out.print("\t");
			}
			for(int j= row ; j >=i; j--){
				System.out.print(num+"\t");
			}
			num--;
			System.out.println();
		}
	}
}
