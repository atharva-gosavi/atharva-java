import java.io.*;
class SpacePattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows:");
		int row = Integer.parseInt(br.readLine());
		for(int i =1; i<=row; i++){
			for(int space= 1 ; space <= row-i; space++){
				System.out.print("\t");
			}
			int num =i;
			for(int j= 1 ; j <= i; j++){
				System.out.print(num+"\t");
				num=num+i;
			}
			System.out.println();
		}
	}
}
