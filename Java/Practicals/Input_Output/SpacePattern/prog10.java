import java.io.*;
class SpacePattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows:");
		int row = Integer.parseInt(br.readLine());
		for(int i =1; i<=row; i++){
			for(int space= 1 ; space <i; space++){
				System.out.print("\t");
			}
			int num = 65+i-1;
			for(int j= row ; j >=i; j--){
				if(i%2==1 && j%2==0){
					System.out.print(num+"\t");
				}
				else if(i%2==1 && j%2==1){
					System.out.print((char)num+"\t");
				}
				else if(i%2==0 && j%2==0){
					System.out.print((char)num+"\t");
				}else{
					System.out.print(num+"\t");
				}
				num++;
			}
			System.out.println();
		}
	}
}
