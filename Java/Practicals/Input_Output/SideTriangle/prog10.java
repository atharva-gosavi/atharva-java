import java.util.*;
class SideTriangle{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rows:");
		int row = sc.nextInt();
		int col = 0;
		for(int i = 1; i<row*2; i++){
			if(i<=row){
				col = row-i;
			}else{
				col = i-row;
			}
			for(int s=1; s<=col; s++){
				System.out.print("\t");
			}
			if(i<=row){
				col=i;
			}
			else{
				col = row*2-i;
			}
			int num =row-col+1;
			for(int j=1; j<=col; j++){
				System.out.print((char)(64+num)+ "\t");
				num++;
			}
			System.out.println();
		}	
	}
}
