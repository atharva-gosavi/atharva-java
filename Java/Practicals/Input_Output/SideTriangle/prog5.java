import java.util.*;
class SideTriangle{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rows:");
		int row = sc.nextInt();
		int col = 0;
		int num =row;
		for(int i = 1; i<row*2; i++){
			if(i<=row){
				col = i;
			}else{
				col--;
			}
			for(int j=1; j<=col; j++){
				System.out.print((char)(num+64)+"\t");
			}
			if(i<row){
				num--;
			}
			else{
				num++;
			}
			System.out.println();
		}	
	}
}
