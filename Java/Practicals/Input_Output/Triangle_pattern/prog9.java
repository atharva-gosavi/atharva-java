import java.io.*;
class Prog9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i = 1; i<=row ; i++){
			int num = row;
			for(int j = row ; j>=i ; j--){
				System.out.print((char)(64+num)+ " " );
				num--;
			}
			System.out.println();
		}

	}
}
