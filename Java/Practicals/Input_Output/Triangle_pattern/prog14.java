import java.io.*;
class Prog14{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i = 1; i<=row ; i++){
			int A = row+64;
			int a = row+96;
			for(int j = 1; j<=i ; j++){
				if(i%2==1){
				System.out.print((char)(a)+" ");
				a--;
				}else{
					System.out.print((char)(A)+" ");
					A--;
				}

			}
			System.out.println();
		}

	}
}
