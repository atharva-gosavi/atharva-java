import java.io.*;
class Prog10{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i = 1; i<=row ; i++){
			int num = 64+i;
			for(int j = row ; j>=i ; j--){
				if(i%2==1 && j%2==0){
				System.out.print(num + " " );
				num++;
				}else if(i%2==1 && j%2==1){		
				System.out.print((char)(num) + " " );
				num++;
				}else if(i%2==0 && j%2==0){
				System.out.print((char)(num) + " " );
				num++;
				}else{
				System.out.print(num + " " );
				num++;
				}

			}
			System.out.println();
		}

	}
}
