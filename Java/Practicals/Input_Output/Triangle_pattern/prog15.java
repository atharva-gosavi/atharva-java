import java.io.*;
class Prog15{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());
		int num = row+65;
		for(int i = 1; i<=row ; i++){
			for(int j = 1; j<=i ; j++){
				System.out.print((char)(num)+" ");
				num++;
			}
			System.out.println();
		}

	}
}
