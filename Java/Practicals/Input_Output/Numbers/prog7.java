import java.io.*;
class Prog7{
	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number: ");
		int num = Integer.parseInt(br.readLine());
		int val = 0;
		int rem = 0;
		int temp = num;
		while(num>0){
			rem = num%10;
			val = val*10 + rem;
			num/=10;
		}System.out.println("Reverse of "+temp+" is "+val);
	}
}
