import java.io.*;
class Prog5{
	public static void main(String[] args)throws IOException{
		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number: ");
		int num = Integer.parseInt(br.readLine());
		int temp = num;
		int i = 1;
		while(num>=1){
			i=i*num;
			num--;
		}
		System.out.println("Factorial of "+temp+" is "+i);
	}
}
