import java.io.*;
class Prog7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());
		int num = 2;
		for(int i =1; i<=row; i++){
			for(int j = 1; j<=row-i+1; j++){
				System.out.print(num+" ");
				num= num+ 2;			
			}
			System.out.println();
		}

	}
}

