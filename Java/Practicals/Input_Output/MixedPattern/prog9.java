import java.io.*;
class Prog9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i =1; i<=row; i++){
			int num = row-i+1;
			int ch = 64 + num;
			for(int j = 1; j<=row-i+1; j++){
				if(i%2==0){
					System.out.print((char)(ch)+" ");
					ch--;
				}else{
					System.out.print(j+" ");			
				}
			}
			System.out.println();
		}

	}
}

