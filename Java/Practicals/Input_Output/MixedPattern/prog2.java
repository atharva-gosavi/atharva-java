import java.io.*;
class Prog2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());
		int ch = 64+row;
		for(int i =1; i<=row; i++){
			int num = row+i-1;
			for(int j = 1; j<=row; j++){
				System.out.print((char)(ch)+""+num+" ");
				num--;
			}
			System.out.println();
		}

	}
}

