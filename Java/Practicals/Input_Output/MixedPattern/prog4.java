import java.io.*;
class Prog4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());
		int num = row;
		for(int i =1; i<=row; i++){
			int temp = num;
			for(int j = 1; j<=i; j++){
				System.out.print(temp+" ");
				temp = num + temp;			
			}
			num--;
			System.out.println();
		}

	}
}

