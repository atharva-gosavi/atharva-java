import java.io.*;
class Prog6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i =1; i<=row; i++){
			int num =row;
			int ch = num + 64;
			for(int j = 1; j<=i; j++){
				if(i%2==1){
					System.out.print((char)(ch)+" ");
				}else{
					System.out.print(num+" ");			
				}
				num--;
				ch--;
			}
			System.out.println();
		}

	}
}

