import java.io.*;
class Prog10{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number: ");
		long num = Long.parseLong(br.readLine());
		long temp = num ;
		long val = 0;
		while(num>0){
			long rem = num%10;
			val = val*10+rem;
			num/=10;
			if(rem%2==1){
				System.out.print(rem*rem + ",");
			}
		}
			System.out.println("Reverse of "+temp+" is "+val);
	}
}


