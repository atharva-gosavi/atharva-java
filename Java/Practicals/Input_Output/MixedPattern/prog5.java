import java.io.*;
class Prog5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());
		for(int i =1; i<=row; i++){
			int num = i;
			for(int j = 1; j<=i; j++){
				System.out.print(num+" ");
				num= num+ i;			
			}
			num--;
			System.out.println();
		}

	}
}

